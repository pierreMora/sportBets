const button = document.querySelector('.btn')
const form   = document.querySelector('.form')



function AffichageErreur(){
	$('.loginIndisp').show();
}

// ----------- POUR LA SELECTION DES PAYS LORS DES TOURS ON VERIFIE QUE LUTILISATEUR NE CHOISI PAS PLUS DE PAYS QU'IL N'EST POSSIBLE ------------

// -- POUR UNE CLASSE DONNEE LA FCT COMPTE CEUX QUI SONT CHECKÉ ----------------

function CountCheckbox(classname,max)
{
	var classnamejquerry = '.'+classname ;
	var checkbox = $(classnamejquerry) ;
	
	var nb = 0 ;

	checkbox.each(function( index ) {
		if ($(this).is(':checked'))
			nb += 1 ;
	});


	return (nb < max)
}

// Lorsque que l'on decoche un pays celui disparait des poules supérieur ( Un Pays n'étant pas présent en huitieme ne peux pas se retrouver en finale par ex)
function uncheckedLowerLevel(tours,id)
{
	var toursarray = ["huitieme", "quart", "demi","finale","gagnant"];
	var position;
	for (var i = 0; i <5; i++) {
		if (tours == toursarray[i])
		{
			position = i; break ;
		}	
	}

	for (var i = position+1;i < 5 ;i++)
	{
		$('#div'+toursarray[i]+id).remove();
	}
}

// Lorsque l'on check un pays on l'envoit décoché dans la poule suivante (ex : on coche un pays en huitieme, on l'envoie en quart pour que l'utilisateur puisse le confirmer ou non)
function checkNextLevel(tours,id,nomPays,nomIMG){
	var toursarray = ["huitieme", "quart", "demi","finale","gagnant"];
	var position;
	for (var i = 0; i <5; i++) {
		if (tours == toursarray[i])
		{
			position = i; break ;
		}
			
	}

	if (position + 1 < 5)
	{
		var max ;
		if (position == 0)
			max = 8 ;
		else if (position == 1)
			max = 4 ;
		else if (position == 2)
			max = 2 ;
		else if (position == 3)
			max = 1 ;

		var paramFonction = id+",'"+toursarray[position+1]+"',"+max+",true,'"+nomPays+"','"+nomIMG+"'" ;

          var element =  '<div  id =   "div'+toursarray[position+1]+id+'" class = "col-md-3 selectionPays" onclick = "wantToCheck('+paramFonction+')">' ;
        
         element +=  '<input type="checkbox" id = "checkbox'+toursarray[position+1]+id+'"  class = "checkbox' + toursarray[position+1]+' " name="checkbox'+toursarray[position+1]+id+'" style = "display:none">' ;

         element +='<img class = "img_flag" src="image/'+nomIMG  + '.png">' ;
         element +='<span class = "nom_pays ellipsis">'+ nomPays + '</span>' ;
                  
         element += "</div>" ;

         $(".groupe"+toursarray[position+1]).append(element) ;

	}
}


// --- LORSQUE L UTILISATEUR CHOISIE UN PAYS ON CHECK OU NON SA CASE EN FONCTION DES CAS ET ON CREE OU SUPPRIME SON OCCURENCE DANS LES AUTRES POULES EN FCT DES CAS
function wantToCheck(id,classname,max,update,nomPays,nomIMG)
{
	// ON TROUVE LES CHECKBOX ASSOCIÉ AUX DIV DES PAYS 
	var idcheck = "#checkbox" + classname + id ;
	checkbox = $(idcheck) ;

	// ON TROUVE LES DIV ASSOCIÉ AUX CHECKBOX
	idcheck = "#div" + classname + id ;
	
	divElement = $(idcheck) ;

	if (checkbox.is(':checked'))
	{
		checkbox.prop('checked', false);
		uncheckedLowerLevel(classname,id) ;
		
		divElement.hover(function(){
    		$(this).css("background-color", "rgba(11,201,224,0.4)");
    	}, function(){
    		$(this).css("background-color", "rgba(200,200,200,0.4)");
		});
		divElement.css("background-color", "rgba(200,200,200,0.4)") ;

	}
	else if (CountCheckbox("checkbox"+classname,max))
	{
		if (update)
			checkNextLevel(classname,id,nomPays,nomIMG);

		checkbox.prop('checked', true);
		divElement.hover(function(){
    		$(this).css("background-color", "rgba(51,255,102,0.8)");
    	}, function(){
    		$(this).css("background-color", "rgba(51,255,102,0.8)");
		});
		divElement.css("background-color", "rgba(51,255,102,0.8)");
	}

}

function makeLink(){
	var idutil ;
	$(".groupePoule").each(function( index ) {
  		
  		
  		$(this).click(function() {
  			idutil = $(this).attr('id') ;
  			document.location.replace('utilisateur.php'+'?id='+idutil);

		});

	});
}