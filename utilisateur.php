<?php
session_start();

 ?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="image/trophy.png"> 

    <title> ForBets</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">

    <link rel="stylesheet" href="css/style_generale.css">

    <script src="jquery/jquery.js"></script>

    <script src="js/fonction.js"></script>

    <script src="bootstrap/js/bootstrap.min.js"></script>
   


  </head>

  <body class="text-center">

    <div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
      <header class="masthead mb-auto">
        <div class="inner">
          <a href="index.php"> <h3 class="masthead-brand"> <img class = "icon" src="image/trophy.png">ForBets</h3> </a>
          <nav class="nav nav-masthead justify-content-center">
            <a class="nav-link" href="index.php">Home</a>
            <a class="nav-link" href="classement.php">Classement</a>

            <a class="nav-link" href="parier.php">Parier</a>

            <a class="nav-link " href="grille.php">Grille</a>
            <a class="nav-link" href="connexion.php">Connexion</a>

           
          </nav>
        </div>
      </header>

      <?php
      /*
        $servername = "127.0.0.1";
        $username = "sportBets";
        $password = "sportBets";
        $dbname = "sportBets" ;
      */
        $servername = "db739631012.db.1and1.com";
        $username = "dbo739631012";
        $password = "DSMF//!sdqx!sqdfdsf4%aqwx";
         $dbname = "db739631012" ;

      mb_internal_encoding('UTF-8');

      try {
          $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
          // set the PDO error mode to exception
          $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          }
      catch(PDOException $e)
          {
          echo "Connection failed: " . $e->getMessage();
          }

          $groupes = $conn->query('SELECT * FROM Groupe');
          $utilisateur = $conn->prepare('SELECT nomUtilisateur, prenomUtilisateur FROM Utilisateur WHERE idUtilisateur = :idUtilisateur');
                  
            $utilisateur->execute(array(
            'idUtilisateur' =>  $_GET['id'] 
            ));
            $utilisateur = $utilisateur->fetch();

        ?>

        <h3 class = "titlePage"> Information Utilisateur </h3>
        <h6 class = "nomUtil"><?php echo ($utilisateur[0]." ".$utilisateur[1] )?> </h6>

        <div class="row">
          
            <?php 
              $i = 1 ;
              while ($groupe = $groupes->fetch())
              {
                  if ($i % 2 == 0)
                  {
                    echo " <div class='col-md-2 separateur'> </div>";
                  }
                  $i = $i + 1 ;
            ?>
          <div class="col-md-5 groupePoule">
                <?php echo '<h4>Goupe ' . $groupe[1] . '</h1>' ?>

                <div class = "row match">
                   <?php
                      // SELECTION DE TOUS LES PARIS DES MATCH PAR GROUPE POUR L UTILISATEUR DONNER
                   
                    $matchs = $conn->prepare('SELECT p1.nom, p1.nomIMG, p2.nom, p2.nomIMG, pa.score1, pa.score2 , m.idMatch FROM Pays p1, Pays p2, Matche m, AssocGroupe aG 
                     ,ParisQualif pa WHERE p1.idPays = m.idPays1 AND p2.idPays = m.idPays2 AND aG.idGroupe = :idGroupe AND aG.idPays = p1.idPays and m.idMatch = pa.idMatche 
                        and pa.idUtilisateur = :idUtilisateur');
                  
                    $matchs->execute(array(

                    'idGroupe' => $groupe[0],
                    'idUtilisateur' =>  $_GET['id'] 
                    ));

                    $matchSave = $matchs ;
                    while ($match = $matchs->fetch())
                    { ?>

                  <div class = "col-md-6" >
                    <div class = "row">
                      <div class = "col-sm-3" >
                      <?php  
                        echo '<img class = "img_flag" src="image/'. $match[1] . '.png">' ;
                      ?>
                      </div>
                      <div class = "col-sm-7" >
                      <?php  
                        echo '<span class = "nom_pays ellipsis">'. $match[0] . '</span>' ;
                      ?>
                      </div>
                      <div class = "col-sm-2" >
                        <?php  
                          echo '<span class = "score" >' . $match[4] . '</span>' ;
                        ?>
                      </div>
                    </div>
                  </div>
                  <div class = "col-md-6" >
                    <div class = "row">
                      <div class = "col-sm-3" >
                        <?php  
                         echo '<span class = "score" >' . $match[5] . '</span>' ;
                      ?>
                      </div>
                      <div class = "col-sm-7" >
                        <?php  
                        echo '<span class = "nom_pays ellipsis">'. $match[2] . '</span>' ;
                      ?>
                      </div>
                      <div class = "col-sm-2" >
                        <?php  
                          echo '<img class = "img_flag" src="image/'. $match[3] . '.png">' ;
                        ?>
                      </div>
                    </div>
                  </div>
                  <?php
                    }
                    $matchs->closeCursor();
                  ?> 
                </div>
          </div>
       

          <?php 
            }
            $groupes->closeCursor();
          ?>
        
        </div> 

       
        <!-- ON SELECTIONNE LES PAYS DEJA PRESENT DANS LES DIFFERENT TOURS -->
        <?php
          $arrayTours = array("huitieme", "quart", "demi", "finale", "gagnant");
          $paysTours = $conn->prepare('SELECT  p.nom,p.nomIMG FROM ParisTours pt, Pays p WHERE p.idPays = pt.idPays and pt.nomTours = :nomTours and pt.idUtilisateur = :idUtilisateur ') ;
           foreach ($arrayTours as &$tours) {
              $paysTours->execute(array('nomTours' => $tours, 'idUtilisateur' => $_GET['id'])) ;
              ?>
                <h5 class = "titlePage"> <?php echo $tours ?> </h5>

                <div class = "row">
                  <div class = "col-md-12 groupePoule">
                    <div class= "row  groupehuitieme">
                      <?php while ($pays = $paysTours->fetch()) { ?>
                      <div class = "col-md-3 selectionPays" >
                      <?php 
                          echo '<img class = "img_flag" src="image/'. $pays[1] . '.png">' ;
                          echo '<span class = "nom_pays ellipsis">'. $pays[0] . '</span>' ;
                      ?>
                      </div>
                      <?php } ?>
                    </div>
                  </div>
                </div>
              <?php
                }
              ?>


      <footer class="mastfoot mt-auto">
        <div class="inner">
          <p>ForBets © Pierre Mora </p>
        </div>
      </footer>
    </div>
  
  </body>
</html>


​

