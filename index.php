<?php
session_start();

 ?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="image/trophy.png"> 

    <title> ForBets</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">

    <link rel="stylesheet" href="css/style_generale.css">

    <script src="jquery/jquery.js"></script>

    <script src="bootstrap/js/bootstrap.min.js"></script>
   


  </head>

  <body class="text-center homePage">

    <div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
      <header class="masthead mb-auto">
        <div class="inner">
          <a href="index.php"> <h3 class="masthead-brand"> <img class = "icon" src="image/trophy.png">ForBets</h3> </a>
          <nav class="nav nav-masthead justify-content-center">
            <a class="nav-link active" href="">Home</a>
            <a class="nav-link" href="classement.php">Classement</a>

            <a class="nav-link " href="parier.php">Parier</a>

            <a class="nav-link" href="grille.php">Grille</a>
            <a class="nav-link" href="connexion.php">
             <?php if ($_SESSION["connect"]) 
                      echo "Deconnexion" ;
                  else
                      echo "Connexion" ;
            ?>
             </a>

           
          </nav>
        </div>
      </header>

      <main role="main" class="inner cover">
        <h1 class="cover-heading">ForBets - Site de paris en ligne.</h1>
        <p class="lead">Parier avec vos amis sur les résultats de la Coupe du Monde de football qui se tiendra prochainement en Russie
        </p>
        <p class="lead">
          <a href="grille.php" class="btn btn-lg btn-secondary">Let's Go</a>
        </p>

      </main>

      <footer class="mastfoot mt-auto">
        <div class="inner">
          <p>ForBets © Pierre Mora </br>
          Icons made by <a href="http://www.freepik.com" title="Freepik">Freepik</a> 
            from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> 
            is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a>
          </p>
        </div>
      </footer>
    </div>

  
  </body>
</html>


​

