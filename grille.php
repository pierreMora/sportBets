<?php
session_start();

 ?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="image/trophy.png"> 

    <title> ForBets</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">

    <link rel="stylesheet" href="css/style_generale.css">

    <script src="jquery/jquery.js"></script>

    <script src="bootstrap/js/bootstrap.min.js"></script>
   


  </head>

  <body class="text-center">

    <div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
      <header class="masthead mb-auto">
        <div class="inner">
          <a href="index.php"> <h3 class="masthead-brand"> <img class = "icon" src="image/trophy.png">ForBets</h3> </a>
          <nav class="nav nav-masthead justify-content-center">
            <a class="nav-link" href="index.php">Home</a>
            <a class="nav-link" href="classement.php">Classement</a>

            <a class="nav-link " href="parier.php">Parier</a>

            <a class="nav-link active" href="">Grille</a>
            <a class="nav-link" href="connexion.php">
             <?php if ($_SESSION["connect"]) 
                      echo "Deconnexion" ;
                  else
                      echo "Connexion" ;
            ?>
             </a>

           
          </nav>
        </div>
      </header>

      <?php
      /*
        $servername = "127.0.0.1";
        $username = "sportBets";
        $password = "sportBets";
        $dbname = "sportBets" ;
      */
        $servername = "db739631012.db.1and1.com";
        $username = "dbo739631012";
        $password = "DSMF//!sdqx!sqdfdsf4%aqwx";
         $dbname = "db739631012" ;

      mb_internal_encoding('UTF-8');

      try {
          $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
          // set the PDO error mode to exception
          $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          }
      catch(PDOException $e)
          {
          echo "Connection failed: " . $e->getMessage();
          }

          $groupes = $conn->query('SELECT * FROM Groupe');



        ?>

        <h3 class = "titlePage"> Coupe du Monde 2018 </h3>
        <p class= "descriptionParis">Voici les résultas <br> Veuillez vous connecter pour effectuer vos paris</p>

        <div class="row">
        
          <?php 
              $i = 1 ;
            while ($groupe = $groupes->fetch())
              {
                  if ($i % 2 == 0)
                  {
                    echo " <div class='col-md-2 separateur'> </div>";
                  }
                  $i = $i + 1 ;
          ?>
          <div class="col-md-5 groupePoule">
                <?php echo '<h4>Goupe ' . $groupe[1] . '</h1>' ?>

                <div class = "row match">
                   <?php
                      // SELECTION DE TOUS LES MATCH PAR GROUPE 
                   
                    $matchs = $conn->prepare('SELECT p1.nom, p1.nomIMG, p2.nom, p2.nomIMG, m.score1, m.score2 ,m.finMatch FROM Pays p1, Pays p2, Matche m, AssocGroupe aG 
                      WHERE p1.idPays = m.idPays1 AND p2.idPays = m.idPays2 AND aG.idGroupe = :idGroupe AND (aG.idPays = p1.idPays )');
                  
                    $matchs->execute(array(

                    'idGroupe' => $groupe[0] 
                    ));
                    while ($match = $matchs->fetch())
                    { ?>

                  <div class = "col-md-6" >
                    <div class = "row">
                      <div class = "col-sm-3" >
                      <?php  
                        echo '<img class = "img_flag" src="image/'. $match[1] . '.png">' ;
                      ?>
                      </div>
                      <div class = "col-sm-7" >
                      <?php  
                        echo '<span class = "nom_pays ellipsis">'. $match[0] . '</span>' ;
                      ?>
                      </div>
                      <div class = "col-sm-2" >
                        <?php  
                          if ($match[6]== 1)
                          echo '<span class = "score">'. $match[4] . '</span>' ;
                        else
                          echo '<span class = "score">-</span>' ;
                        ?>
                      </div>
                    </div>
                  </div>
                  <div class = "col-md-6" >
                    <div class = "row">
                      <div class = "col-sm-3" >
                        <?php  
                        if ($match[6]== 1)
                          echo '<span class = "score">'. $match[5] . '</span>' ;
                        else
                          echo '<span class = "score">-</span>' ;

                      ?>
                      </div>
                      <div class = "col-sm-7" >
                        <?php  
                        echo '<span class = "nom_pays ellipsis">'. $match[2] . '</span>' ;
                      ?>
                      </div>
                      <div class = "col-sm-2" >
                        <?php  
                          echo '<img class = "img_flag" src="image/'. $match[3] . '.png">' ;
                        ?>
                      </div>
                    </div>
                  </div>
                  <?php
                    }
                    $matchs->closeCursor();
                  ?> 
                </div>
          </div>

          <?php 
            }
            $groupes->closeCursor();
          ?>
        </div> 

     

               <!-- CHOIX DES PAYS ETANT EN HUITIEME DE FINAL -->
        <h5 class = "titlePage"> Huitième </h5>

        <div class = "row">
          <div class = "col-md-12 groupePoule">
            <div class= "row groupehuitieme">
              <?php 
                $pays = $conn->query('SELECT p.idPays , p.nom, p.nomIMG FROM Pays p, Tours t WHERE t.idPays = p.idPays and t.nomTours = "huitieme" ') ;
                while ($findPays = $pays->fetch())
                {
                 

                  echo '<div  id =   "divhuitieme'.$findPays[0].'" class = "col-md-3 grillePays" >' ;
        
                  echo '<img class = "img_flag" src="image/'. $findPays[2] . '.png">' ;
                  echo '<span class = "nom_pays ellipsis">'. $findPays[1] . '</span>' ;
                  
                  echo "</div>" ;
                }
              ?>
            </div>
          </div>
        </div>

        <!-- CHOIX DES PAYS ETANT EN QUART FINAL -->
         <h5 class = "titlePage"> Quart </h5>

        <div class = "row">
          <div class = "col-md-12 groupePoule">
            <div class= "row  groupequart">
              <?php 
                $pays = $conn->query('SELECT p.idPays , p.nom, p.nomIMG FROM Pays p, Tours t WHERE t.idPays = p.idPays and t.nomTours = "quart" ') ;
                while ($findPays = $pays->fetch())
                {

                  
                  echo '<div  id =   "divquart'.$findPays[0].'" class = "col-md-3 grillePays">' ;
        
                  
                  echo '<img class = "img_flag" src="image/'. $findPays[2] . '.png">' ;
                  echo '<span class = "nom_pays ellipsis">'. $findPays[1] . '</span>' ;
                  
                  echo "</div>" ;
                }
              ?>
            </div>
          </div>
        </div>

        <!-- CHOIX DES PAYS ETANT EN DEMI FINAL -->
         <h5 class = "titlePage"> Demi Finale </h5>

        <div class = "row">
          <div class = "col-md-12 groupePoule">
            <div class= "row groupedemi">
              <?php 
               $pays = $conn->query('SELECT p.idPays , p.nom, p.nomIMG FROM Pays p, Tours t WHERE t.idPays = p.idPays and t.nomTours = "demi" ') ;
                while ($findPays = $pays->fetch())
                {

                  echo '<div  id =   "divdemi'.$findPays[0].'" class = "col-md-3 grillePays" >' ;
        
                  echo '<img class = "img_flag" src="image/'. $findPays[2] . '.png">' ;
                  echo '<span class = "nom_pays ellipsis">'. $findPays[1] . '</span>' ;
                  
                  echo "</div>" ;
                }
              ?>
            </div>
          </div>
        </div>

        <!-- CHOIX DES PAYS ETANT EN FINAL -->
         <h5 class = "titlePage"> Finale </h5>

        <div class = "row">
          <div class = "col-md-12 groupePoule">
            <div class= "row groupefinale">
              <?php 
               $pays = $conn->query('SELECT p.idPays , p.nom, p.nomIMG FROM Pays p, Tours t WHERE t.idPays = p.idPays and t.nomTours = "finale" ') ;
                while ($findPays = $pays->fetch())
                {
                  echo '<div  id =   "divfinale'.$findPays[0].'" class = "col-md-3 grillePays" >' ;
        
                  echo '<img class = "img_flag" src="image/'. $findPays[2] . '.png">' ;
                  echo '<span class = "nom_pays ellipsis">'. $findPays[1] . '</span>' ;
                  
                  echo "</div>" ;
                }
                
              ?>
            </div>
          </div>
        </div>

         <!-- CHOIX DES PAYS ETANT EN FINAL -->
         <h5 class = "titlePage"> Champion du monde 2018 </h5>

        <div class = "row">
          <div class = "col-md-12 groupePoule">
            <div class= "row groupegagnant">
              <div class = "col-md-2"></div>
              <?php 
               $pays = $conn->query('SELECT p.idPays , p.nom, p.nomIMG FROM Pays p, Tours t WHERE t.idPays = p.idPays and t.nomTours = "gagnant" ') ;
                while ($findPays = $pays->fetch())
                {

                 

                  echo '<div  id =   "divgagnant'.$findPays[0].'" class = "col-md-8 grillePays" style = "max-width:none">' ;
        
                  echo '<input type="checkbox" id = "checkboxgagnant'.$findPays[0].'"  class = "checkboxgagnant" name="checkboxgagnant'.$findPays[0].'" style = "display:none">' ;

                  echo '<img class = "img_flag" src="image/'. $findPays[2] . '.png">' ;
                  echo '<span class = "nom_pays ellipsis">'. $findPays[1] . '</span>' ;
                  
                  echo "</div>" ;
                }
                $conn = null ;
              ?>
               <div class = "col-md-2"></div>
            </div>
          </div>
        </div>

   

      <footer class="mastfoot mt-auto">
        <div class="inner">
          <p>ForBets © Pierre Mora </p>
        </div>
      </footer>
    </div>


  
  </body>
</html>


​

