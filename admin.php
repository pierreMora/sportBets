<?php
session_start();
  // SI ON EST PAS ADMIN ON NE PEUT PAS RENTRER DANS CETTE PAGE
 if ($_SESSION["admin"] != true)
 {
   echo "<script type='text/javascript'>document.location.replace('connexion.php');</script>";
  }
 ?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="image/trophy.png"> 

    <title> ForBets</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">

    <link rel="stylesheet" href="css/style_generale.css">

    <script src="jquery/jquery.js"></script>

    <script src="js/fonction.js"></script>

    <script src="bootstrap/js/bootstrap.min.js"></script>
   


  </head>

  <body class="text-center">

    <div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
      <header class="masthead mb-auto">
        <div class="inner">
          <a href="index.php"> <h3 class="masthead-brand"> <img class = "icon" src="image/trophy.png">ForBets</h3> </a>
          <nav class="nav nav-masthead justify-content-center">
            <a class="nav-link" href="index.php">Home</a>
            <a class="nav-link" href="#">Classement</a>

            <a class="nav-link " href="parier.php">Parier</a>

            <a class="nav-link" href="grille.php">Grille</a>
            <a class="nav-link " href="connexion.php">Deconnexion</a>

           
          </nav>
        </div>
      </header>

      <?php
      /*
        $servername = "127.0.0.1";
        $username = "sportBets";
        $password = "sportBets";
        $dbname = "sportBets" ;
      */
        $servername = "db739631012.db.1and1.com";
        $username = "dbo739631012";
        $password = "DSMF//!sdqx!sqdfdsf4%aqwx";
         $dbname = "db739631012" ;

      mb_internal_encoding('UTF-8');

      try {
          $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
          // set the PDO error mode to exception
          $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          }
      catch(PDOException $e)
          {
          echo "Connection failed: " . $e->getMessage();
          }


          $groupes = $conn->query('SELECT * FROM Groupe');
          $huitieme = $conn->query('SELECT * FROM Tours WHERE nomTours = "huitieme"');

        ?>

        <h3 class = "titlePage"> Espace Administrateur </h3>
        <h5 class = "titlePage"> Poules </h5>
         <form id="form" action="#" method="POST">
        <div class="row">
          
            <?php 
              $i = 1 ;
              while ($groupe = $groupes->fetch())
              {
                  if ($i % 2 == 0)
                  {
                    echo " <div class='col-md-2 separateur'> </div>";
                  }
                  $i = $i + 1 ;
            ?>
          <div class="col-md-5 groupePoule">
                <?php echo '<h4>Goupe ' . $groupe[1] . '</h1>' ?>

                <div class = "row match">
                   <?php
                      // SELECTION DE TOUS LES MATCH PAR GROUPE 
                   
                    $matchs = $conn->prepare('SELECT p1.nom, p1.nomIMG, p2.nom, p2.nomIMG, m.score1, m.score2 , m.idMatch, m.finMatch FROM Pays p1, Pays p2, Matche m, AssocGroupe aG 
                      WHERE p1.idPays = m.idPays1 AND p2.idPays = m.idPays2 AND aG.idGroupe = :idGroupe AND (aG.idPays = p1.idPays )');
                  
                    $matchs->execute(array(

                    'idGroupe' => $groupe[0] 
                    ));

                    $matchSave = $matchs ;
                    while ($match = $matchs->fetch())
                    { ?>

                  <div class = "col-md-6" >
                    <div class = "row">
                      <div class = "col-sm-3" >
                      <?php  
                        echo '<img class = "img_flag" src="image/'. $match[1] . '.png">' ;
                      ?>
                      </div>
                      <div class = "col-sm-7" >
                      <?php  
                        echo '<span class = "nom_pays ellipsis">'. $match[0] . '</span>' ;
                      ?>
                      </div>
                      <div class = "col-sm-2" >
                        <?php  
                          if ($match[7] == 0)
                          {
                            echo '<input id = "score" class = "score" name = "score1'.$match[6].'" placeholder = "-">' ;
                          }
                          else
                          {
                            echo '<input id = "score" class = "score" name = "score1'.$match[6].'" value = '. $match[4] . '>' ;
                          }

                          
                        ?>
                      </div>
                    </div>
                  </div>
                  <div class = "col-md-6" >
                    <div class = "row">
                      <div class = "col-sm-3" >
                        <?php  
                         if ($match[7] == 0)
                          {
                            echo '<input id = "score" class = "score" name = "score2'.$match[6].'" placeholder = "-">' ;
                          }
                          else
                          {
                            echo '<input id = "score" class = "score" name = "score2'.$match[6].'" value = '. $match[5] . '>' ;
                          }
                      ?>
                      </div>
                      <div class = "col-sm-7" >
                        <?php  
                        echo '<span class = "nom_pays ellipsis">'. $match[2] . '</span>' ;
                      ?>
                      </div>
                      <div class = "col-sm-2" >
                        <?php  
                          echo '<img class = "img_flag" src="image/'. $match[3] . '.png">' ;
                        ?>
                      </div>
                    </div>
                  </div>
                  <?php
                    }
                    $matchs->closeCursor();
                  ?> 
                </div>
          </div>
       

          <?php 
            }
            $groupes->closeCursor();
          ?>
        
        </div> 

        <!-- CHOIX DES PAYS ETANT EN HUITIEME DE FINAL -->

        <h5 class = "titlePage"> Huitième </h5>

        <div class = "row">
          <div class = "col-md-12 groupePoule">
            <div class= "row  groupehuitieme">
              <?php 
                $pays = $conn->query('SELECT * FROM Pays') ;
                while ($findPays = $pays->fetch())
                {
                  

                  $paramFonction = $findPays[0].",'huitieme',16,true,'".$findPays[1]."','".$findPays[2]."'" ;

                  echo '<div  id =   "divhuitieme'.$findPays[0].'" class = "col-md-3 selectionPays" onclick = "wantToCheck('.$paramFonction.')">' ;
        
                  echo '<input type="checkbox" id = "checkboxhuitieme'.$findPays[0].'"  class = "checkboxhuitieme" name="checkboxhuitieme'.$findPays[0].'" style = "display:none">' ;

                  echo '<img class = "img_flag" src="image/'. $findPays[2] . '.png">' ;
                  echo '<span class = "nom_pays ellipsis">'. $findPays[1] . '</span>' ;
                  
                  echo "</div>" ;
                }
              ?>
            </div>
          </div>
        </div>

        <!-- CHOIX DES PAYS ETANT EN QUART DE FINAL -->
        <h5 class = "titlePage"> Quart </h5>

        <div class = "row">
          <div class = "col-md-12 groupePoule">
            <div class= "row groupequart">
              <?php 
                $pays = $conn->query('SELECT p.idPays , p.nom, p.nomIMG FROM Pays p, Tours t WHERE t.idPays = p.idPays and t.nomTours = "huitieme" ') ;
                while ($findPays = $pays->fetch())
                {
                 

                  $paramFonction = $findPays[0].",'quart',8,true,'".$findPays[1]."','".$findPays[2]."'" ;

                  echo '<div  id =   "divquart'.$findPays[0].'" class = "col-md-3 selectionPays" onclick = "wantToCheck('.$paramFonction.')">' ;
        
                  echo '<input type="checkbox" id = "checkboxquart'.$findPays[0].'"  class = "checkboxquart" name="checkboxquart'.$findPays[0].'" style = "display:none">' ;

                  echo '<img class = "img_flag" src="image/'. $findPays[2] . '.png">' ;
                  echo '<span class = "nom_pays ellipsis">'. $findPays[1] . '</span>' ;
                  
                  echo "</div>" ;
                }
              ?>
            </div>
          </div>
        </div>

        <!-- CHOIX DES PAYS ETANT EN DEMI FINAL -->
         <h5 class = "titlePage"> Demi Finale </h5>

        <div class = "row">
          <div class = "col-md-12 groupePoule">
            <div class= "row  groupedemi">
              <?php 
                $pays = $conn->query('SELECT p.idPays , p.nom, p.nomIMG FROM Pays p, Tours t WHERE t.idPays = p.idPays and t.nomTours = "quart" ') ;
                while ($findPays = $pays->fetch())
                {

                  $paramFonction = $findPays[0].",'demi',4,true,'".$findPays[1]."','".$findPays[2]."'" ;

                  echo '<div  id =   "divdemi'.$findPays[0].'" class = "col-md-3 selectionPays" onclick = "wantToCheck('.$paramFonction.')">' ;
        
                  echo '<input type="checkbox" id = "checkboxdemi'.$findPays[0].'"  class = "checkboxdemi" name="checkboxdemi'.$findPays[0].'" style = "display:none">' ;

                  echo '<img class = "img_flag" src="image/'. $findPays[2] . '.png">' ;
                  echo '<span class = "nom_pays ellipsis">'. $findPays[1] . '</span>' ;
                  
                  echo "</div>" ;
                }
              ?>
            </div>
          </div>
        </div>

        <!-- CHOIX DES PAYS ETANT EN FINAL -->
         <h5 class = "titlePage"> Finale </h5>

        <div class = "row">
          <div class = "col-md-12 groupePoule">
            <div class= "row groupefinale">
              <?php 
               $pays = $conn->query('SELECT p.idPays , p.nom, p.nomIMG FROM Pays p, Tours t WHERE t.idPays = p.idPays and t.nomTours = "demi" ') ;
                while ($findPays = $pays->fetch())
                {

                  $paramFonction = $findPays[0].",'finale',2,true,'".$findPays[1]."','".$findPays[2]."'" ;

                  echo '<div  id =   "divfinale'.$findPays[0].'" class = "col-md-3 selectionPays" onclick = "wantToCheck('.$paramFonction.')">' ;
        
                  echo '<input type="checkbox" id = "checkboxfinale'.$findPays[0].'"  class = "checkboxfinale" name="checkboxfinale'.$findPays[0].'" style = "display:none">' ;

                  echo '<img class = "img_flag" src="image/'. $findPays[2] . '.png">' ;
                  echo '<span class = "nom_pays ellipsis">'. $findPays[1] . '</span>' ;
                  
                  echo "</div>" ;
                }
              ?>
            </div>
          </div>
        </div>

        <!-- CHOIX DES PAYS ETANT EN FINAL -->
         <h5 class = "titlePage"> Champion du Monde 2018 </h5>

        <div class = "row">
          <div class = "col-md-12 groupePoule">
            <div class= "row groupegagnant">
              <?php 
               $pays = $conn->query('SELECT p.idPays , p.nom, p.nomIMG FROM Pays p, Tours t WHERE t.idPays = p.idPays and t.nomTours = "finale" ') ;
                while ($findPays = $pays->fetch())
                {

                  $paramFonction = $findPays[0].",'gagnant',2,true,'".$findPays[1]."','".$findPays[2]."'" ;

                  echo '<div  id =   "divgagnant'.$findPays[0].'" class = "col-md-3 selectionPays" onclick = "wantToCheck('.$paramFonction.')">' ;
        
                  echo '<input type="checkbox" id = "checkboxgagnant'.$findPays[0].'"  class = "checkboxgagnant" name="checkboxgagnant'.$findPays[0].'" style = "display:none">' ;

                  echo '<img class = "img_flag" src="image/'. $findPays[2] . '.png">' ;
                  echo '<span class = "nom_pays ellipsis">'. $findPays[1] . '</span>' ;
                  
                  echo "</div>" ;
                }
              ?>
            </div>
          </div>
        </div>


       
        <!-- ON SELECTIONNE LES PAYS DEJA PRESENT DANS LES DIFFERENT TOURS -->
        <?php
          $arrayTours = array("huitieme", "quart", "demi", "finale", "gagnant");
          $paysTours = $conn->prepare('SELECT idPays FROM Tours WHERE nomTours = :nomTours ') ;
           foreach ($arrayTours as &$tours) {
              $paysTours->execute(array('nomTours' => $tours)) ;
              $i=16 ;
               while($pays = $paysTours->fetch())
              {
                $paramFonction = $pays[0].",'".$tours."',".$i.",false,'',''";
                echo "<script>wantToCheck(".$paramFonction.")</script>" ;
              }
           }

           $cloture = $conn->query('SELECT cloture FROM FinDesParis WHERE idFinDesParis = 1') ;
           $finParis = $cloture->fetch();
           
           if ($finParis[0])
           {
            echo ' <input class="clotureBoutton" name = "submitButtonOpen" type="submit" value="Ouvrir les paris" >' ;
           }
            
            else
            {
              echo ' <input class="clotureBoutton" name = "submitButtonCloture" type="submit" value="Cloturer les paris" >' ;

            }
        ?>


          <input class="submitButton" name = "submitButton" type="submit" value="Modifier les résultats">

        </form>

        <?php

           if (isset($_POST["submitButtonCloture"]))
           {
              $conn->query('UPDATE FinDesParis SET cloture = 1 WHERE idFinDesParis = 1') ;
               $conn = null ;
              echo "<script type='text/javascript'>document.location.replace('admin.php');</script>";

           }
          else if (isset($_POST["submitButtonOpen"]))
          {
             $conn->query('UPDATE FinDesParis SET cloture = 0 WHERE idFinDesParis = 1') ;
              $conn = null ;
              echo "<script type='text/javascript'>document.location.replace('admin.php');</script>";
          }
          else if (isset($_POST["submitButton"]))
          {

            //------------------- ACTUALISATION DES SCORES MATCHS --------------------------------
           
              $matchs = $conn->query('SELECT * FROM Matche');
            
              while ($match= $matchs->fetch()) {
                // echo ("<script>alert(".$match[0].")</script>") ;
                $updateScore =  $conn->prepare('UPDATE Matche SET score1 = :score1 , score2 =:score2, finMatch = true WHERE idMatch = :idMatch') ;
                $namescore1 = 'score1'.$match[0] ;
                $namescore2 = 'score2'.$match[0] ;

                // ON TEST SI LES DONNEES RENTRÉ PAR L ADMIN SONT CORRECTE

                if ((is_numeric($_POST[$namescore1]))&&(is_numeric($_POST[$namescore2])))
                {
                  
                    $updateScore->execute(array(
                    'score1' => $_POST[$namescore1],
                    'score2' => $_POST[$namescore2],
                    'idMatch' => $match[0]

                    ));
                    
                }  
              }

            //---------------------- ACTUALISATION DES PAYS DANS LES DIFFERENTS TOURS ----------------------------
          
            $arrayTours = array("huitieme", "quart", "demi", "finale", "gagnant");
           
            
            $deleteTours = $conn->prepare('DELETE FROM Tours where nomTours = :nomTours') ;
            $updateTours =  $conn->prepare('INSERT INTO Tours(idPays,nomTours) values(:idpays,:nomTours)');


            foreach ($arrayTours as &$tours) {
              $deleteTours->execute(array('nomTours' => $tours)) ;
              echo "<script>console.debug('".$tours."')</script>" ;
              $pays = $conn->query('SELECT idPays FROM Pays');
              while($paysSelect = $pays->fetch())
              {
                
                $select = "checkbox".$tours.$paysSelect[0] ;

                
                if (isset($_POST[$select]))
                {
                  
                  $updateTours->execute(array(
                    'idpays' => $paysSelect[0],
                    ':nomTours' => $tours
                    )) ;
                }
              }

            }

            

            //------------------- ACTUALISATION DES POINTS UTILISATEURS --------------------------------
           

            $utilisateurs = $conn->query('SELECT idUtilisateur FROM Utilisateur');

            while ($utilisateur = $utilisateurs->fetch()) {

                  // -------------- UPDATE DES POINTS UTILISATEUR POUR LES MATCH DE POULES -------------------
                  $paris =  $conn->prepare('SELECT p.score1 , p.score2, m.score1, m.score2 FROM Matche m, ParisQualif p WHERE p.idUtilisateur = :idUtil 
                              AND m.idMatch = p.idMatche AND m.finMatch = 1') ;

                  $scoreutilisateur = 0 ;

                  $paris->execute(array('idUtil'=>$utilisateur[0]  ));

                    while ($pari = $paris->fetch())
                    {
                        // TOUT BON
                        if (($pari[0] == $pari[2])&&($pari[1] == $pari[3]))
                          $scoreutilisateur += 5 ;
                        else
                        {
                          // ON A LE GAGNANT 
                          if ((($pari[0] < $pari[1]) && ($pari[2] < $pari[3]))
                            || (($pari[0] > $pari[1]) && ($pari[2] > $pari[3]))
                            || (($pari[0] == $pari[1]) && ($pari[2] == $pari[3]))
                            )
                          {
                            // BONNE DIFFERENCE DE POINTS
                            if (abs($pari[0] -$pari[1]) == abs($pari[2] -$pari[3]) )
                              $scoreutilisateur += 3 ;
                            else
                              $scoreutilisateur += 2 ;
                          }
                        }

                    }

                    //--------------------ACTUALISATION DES POINTS UTILISATEUR POUR LES TOURS -----------------------------

                      $arrayTours = array("huitieme", "quart", "demi", "finale", "gagnant");
                      $scoreTours = array(1,2,3,5,10) ;
                   
                    
                      $selectionParis = $conn->prepare('SELECT idPays   FROM ParisTours  WHERE idUtilisateur = :idUtilisateur AND nomTours = :nomTours ') ;
                      $selectionReel =  $conn->prepare('SELECT COUNT(idTours) as nbOccur   FROM Tours  WHERE nomTours = :nomTours AND idPays = :idPays ');

                      $i = 0 ;
                      $scoreUtilisateurTours = 0 ;
                      foreach ($arrayTours as &$tours) {

                          $selectionParis->execute(array('idUtilisateur'=>$utilisateur[0] , 'nomTours' => $tours)) ;

                          while ($paysParis = $selectionParis->fetch()) {
                                $selectionReel->execute(array('nomTours' => $tours, 'idPays' => $paysParis[0])) ;
                                $result = $selectionReel->fetch() ;
                                $scoreUtilisateurTours += ($scoreTours[$i]) * $result['nbOccur'] ;

                          }

                          $i += 1 ;

                      }

                          

                    //--------------- SOMME DE TOUS LES POINTS ET UPDATE DE LA BDD --------------------------------
                    $scoreutilisateur += $scoreUtilisateurTours ;
                          
                    $updatePoints = $conn->prepare('UPDATE Utilisateur SET points = :points WHERE idUtilisateur = :idUtil') ;
                    $updatePoints->execute(array(
                    'points' => $scoreutilisateur,
                    'idUtil' => $utilisateur[0]
                    ));

                }
            

          
            $conn = null ;
            echo "<script type='text/javascript'>document.location.replace('admin.php');</script>";
           
          }


        ?>
   

      <footer class="mastfoot mt-auto">
        <div class="inner">
          <p>ForBets © Pierre Mora </p>
        </div>
      </footer>
    </div>
  
  </body>
</html>


​

