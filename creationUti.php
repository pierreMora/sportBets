<?php
  session_start();
  // On détruit les variables de notre session
  session_unset ();
  // On détruit notre session
  //session_destroy ();
 ?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'>
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="image/trophy.png"> 

    <title> ForBets</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">

    <link rel="stylesheet" href="css/style_generale.css">
    <link rel="stylesheet" href="css/form.css">

    <script src="jquery/jquery.js"></script>
        <script src="js/fonction.js"></script>


    <script src="bootstrap/js/bootstrap.min.js"></script>
   


  </head>

  <body class="text-center">

    <div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
      <header class="masthead mb-auto">
        <div class="inner">
          <a href="index.php"> <h3 class="masthead-brand"> <img class = "icon" src="image/trophy.png">ForBets</h3> </a>
          <nav class="nav nav-masthead justify-content-center">
             <a class="nav-link" href="index.php">Home</a>
            <a class="nav-link" href="classement.php">Classement</a>

            <a class="nav-link " href="parier.php">Parier</a>

            <a class="nav-link" href="grille.php">Grille</a>
            <a class="nav-link active" href="">Connexion</a>

           
          </nav>
        </div>
      </header>
    
   <div class="user">
    <div class="alert alert-danger loginIndisp" role="alert" style = "display:none">
      Login non disponnible 
    </div>
    <header class="user__header">
        <img src="image/add-user-128.png" style="width:80px;height:80px" alt="" />
        <h1 class="user__title">Création de votre compte</h1>
    </header>
   
    <form class="form" action="#" method="POST">
        <div class="form__group">
            <input type="text" placeholder="Nom" name="nom" class="form__input" required />
        </div>
        <div class="form__group">
            <input type="text" placeholder="Prenom" name="prenom" class="form__input" required />
        </div>

        <div class="form__group">
            <input type="text" placeholder="Login" name="login" class="form__input" required />
        </div>
        
        
        <div class="form__group">
            <input type="password" placeholder="Password" name = "password" class="form__input"  required/>
        </div>
        
        <button class="btn" name="submit" type="submit">Register</button>
    </form>
    </div>

      <a id = "inscription" href="connexion.php"> Déjà membre ? </a>


      <?php
      /*
        $servername = "127.0.0.1";
        $username = "sportBets";
        $password = "sportBets";
        $dbname = "sportBets" ;
      */
        $servername = "db739631012.db.1and1.com";
        $username = "dbo739631012";
        $password = "DSMF//!sdqx!sqdfdsf4%aqwx";
         $dbname = "db739631012" ;

      mb_internal_encoding('UTF-8');

      try {
          $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
          
          $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          }
      catch(PDOException $e)
          {
          echo "Connection failed: " . $e->getMessage();
          }

          // CHECK SI LE LOGIN EST LIBRE

          $req = $conn->query('SELECT  login FROM Utilisateur') ;
          $erreurLogin = false ;

             while($utilisateur = $req->fetch())
             {
              if ($utilisateur["login"] == $_POST['login'])
              {
                $erreurLogin = true ;
                break;
              } 
             }

          if ($erreurLogin)
          {
            echo "<script>AffichageErreur()</script>" ;
          }
          else
          {
                      // ENREGISTREMENT D UN NOUVEL UTILISATEUR

            if (isset($_POST["submit"]))
              {
              
                $req = $conn->prepare('INSERT INTO Utilisateur(nomUtilisateur, prenomUtilisateur, login, password)
                VALUES(:nom, :prenom, :login, :password)');

                $req->execute(array(

                'nom' =>  $_POST['nom'],

                'prenom' => $_POST['prenom'],

                'login' => $_POST['login'],

                'password' => md5($_POST['password']) // MD5 hache le mot de passe pour qu'il ne soit pas représenté en clair dans la base de données  

                ));


                
                // RECUPERATION DU NOUVEL IDENTIFIAN DE L UTILISATEUR CREER
                $req = $conn->query('SELECT idUtilisateur , login FROM Utilisateur') ;

                 while($utilisateur = $req->fetch())
                 {
                 
                  if ($utilisateur["login"] == $_POST['login'])
                  {
                    $idNew = $utilisateur["idUtilisateur"] ;
                    break;
                  }
                    
                 }

                 // INITIALISATION DE TOUT CES PARIS A 0 - 0 POUR CHAQUE MATCHE

                 $req = $conn->query('SELECT idMatch FROM Matche') ;
                 $parie =  $conn->prepare('INSERT INTO ParisQualif(idMatche,idUtilisateur,score1,score2 )
                                          VALUES(:idMatch, :idUser, :score1, :score2)') ;

                 while($match = $req->fetch())
                 {
                   
                   $parie->execute(array(
                    'score1' => '0',
                    'score2' => '0',
                    'idMatch' => $match["idMatch"],
                    'idUser' => $idNew

                    ));
                    
                 }

                  $_SESSION["connect"] = true ;
                  $_SESSION["idUtilisateur"] = $idNew;
                  $_SESSION["nomUtilisateur"] =  $_POST["nom"] ;
                  $_SESSION["prenomUtilisateur"] =  $_POST["prenom"] ;
                  $_SESSION["admin"] =  false;
                
                  echo "<script type='text/javascript'>document.location.replace('parier.php');</script>";
                 
              }
          }



          $conn = null;

        ?>

     

      <footer class="mastfoot mt-auto">
        <div class="inner">
          <p>ForBets © Pierre Mora </p>
        </div>
      </footer>
    </div>


  
  </body>
</html>


​

